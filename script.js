const button = document.querySelector(".enviar")

button.addEventListener('click', event => {
    const messages = document.querySelector('ul')
    const txtarea = document.querySelector('.text')
    const li = document.createElement('li')
    li.classList.add('message')

    li.innerHTML = `<p>${txtarea.value}<p>
            <div class="botoes">
                <input type="button" class="Editar botao-msg" value="Editar" onclick="editar(this)">
                <input type="button" class="Deletar botao-msg" value="Deletar" onclick="deletar(this)">
            </div>
    `


    txtarea.value = ""
    messages.appendChild(li)

})

function deletar(el){
    el.parentNode.parentNode.remove()
}